﻿using System;


namespace BufferCodeGen
{
    class Program
    {
        static void Main(string[] args)
        {

            switch (args[0])
            {
                case "adlgen":
                    SerializerGenerator.Run(args[1], args[2]);
                    break;

                case "hashgen":
                    HashGenerator.Generate(args[1], args[2]);
                    break;

                default:
                    Console.WriteLine($"Unrecognized Command : {args[0]}");
                    break;
            }
        }
    }
}
