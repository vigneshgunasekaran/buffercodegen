﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BufferCodeGen
{
    public static class HashGenerator
    {
        public static void Generate(string typesJSON, string assetsLocation)
        {
            string jsonString = File.ReadAllText(typesJSON);

            string[] m_AssetTypes = JsonConvert.DeserializeObject<List<string>>(jsonString).ToArray(); 
            for (int i = 0; i < m_AssetTypes.Length; i++)
                m_AssetTypes[i] = string.Concat(".", m_AssetTypes[i]);

            string[] filePaths = FilterFiles(assetsLocation, m_AssetTypes).ToArray();
            foreach (var item in filePaths)
            {
                string contents = File.ReadAllText(item);
                contents = contents.Replace("{", string.Concat("{\n", string.Format("\t\"assetID\" : {0},", item.GetStableHashCode())));
                File.WriteAllText(item, contents);
                Console.WriteLine($"{item}  {item.GetStableHashCode()}");

            }
        }

        private static  IEnumerable<string> FilterFiles(string path, params string[] exts)
        {
            return
                Directory
                .EnumerateFiles(path, "*.*", SearchOption.AllDirectories)
                .Where(file => exts.Any(x => file.EndsWith(x, StringComparison.OrdinalIgnoreCase)));
        }
    }

    public static class StringExtensionMethods
    {
        public static int GetStableHashCode(this string str)
        {
            unchecked
            {
                int hash1 = 5381;
                int hash2 = hash1;

                for (int i = 0; i < str.Length && str[i] != '\0'; i += 2)
                {
                    hash1 = ((hash1 << 5) + hash1) ^ str[i];
                    if (i == str.Length - 1 || str[i + 1] == '\0')
                        break;
                    hash2 = ((hash2 << 5) + hash2) ^ str[i + 1];
                }

                return hash1 + (hash2 * 1566083941);
            }
        }
    }
}
