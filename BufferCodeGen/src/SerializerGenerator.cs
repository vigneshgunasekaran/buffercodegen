﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BufferCodeGen
{

    public static class SerializerGenerator
    {

        private struct SerializableType
        {
            public string Name;
            public string Include;
            public bool Copyable;
            public Argument[] Args;
        }

        private struct Argument
        {
            public string Name;
            public string Type;
        }

        public static void Run(string typesJSON, string outputLocation)
        {

            StringBuilder m_CodeBuilder = new StringBuilder();
            StringBuilder m_ArgBuilder = new StringBuilder();


            string jsonString = File.ReadAllText(typesJSON);

            List<SerializableType> m_SerializableTypes = JsonConvert.DeserializeObject<List<SerializableType>>(jsonString);
            HashSet<string> m_Includes = new HashSet<string>();

            m_CodeBuilder.Append("#pragma once");
            m_CodeBuilder.Append("\n#include \"nlohmann/json.hpp\"");
            foreach (var type in m_SerializableTypes)
            {
                if (type.Include != null && type.Include != "")
                {
                    if (!m_Includes.Contains(type.Include))
                    {
                        m_Includes.Add(type.Include);
                        m_CodeBuilder.Append($"\n#include \"{type.Include}\"");
                    }
                }
            }

            m_CodeBuilder.Append("\n\n");
            m_CodeBuilder.Append("namespace nlohmann\n\n{");

            foreach (var type in m_SerializableTypes)
            {
                AppendSpecializations(m_CodeBuilder, m_ArgBuilder, type);
            }

            m_CodeBuilder.Append("\n}");

            Console.Write(m_CodeBuilder.ToString());
            File.WriteAllText(outputLocation, m_CodeBuilder.ToString());
        }


        private static void AppendSpecializations(StringBuilder codeBuilder, StringBuilder argBuilder, SerializableType type)
        {
            argBuilder.Clear();

            codeBuilder.Append("\n\n\ttemplate <>");
            codeBuilder.Append($"\n\tstruct adl_serializer<{type.Name}>").Append("\n\t{");

            codeBuilder.Append($"\n\t\tstatic void to_json(json& j, const {type.Name}& obj)").Append("\n\t\t{");
            codeBuilder.Append("\n\t\t\tj = json{");

            for (int i = 0; i < type.Args.Length; i++)
            {
                codeBuilder.Append($"\n\t\t\t        \t{{\"{type.Args[i].Name}\", obj.{type.Args[i].Name}}}");
                if (i < type.Args.Length - 1) codeBuilder.Append(",");
            }

            codeBuilder.Append("\n\t\t\t        };");
            codeBuilder.Append("\n\t\t}");

            if (type.Copyable)
            {
                codeBuilder.Append($"\n\n\t\tstatic void from_json(const json& j, {type.Name}& obj)").Append("\n\t\t{");
                foreach (var arg in type.Args)
                {
                    codeBuilder.Append($"\n\t\t\tj.at(\"{arg.Name}\").get_to(obj.{arg.Name});");
                }
            }
            else
            {
                codeBuilder.Append($"\n\n\t\tstatic {type.Name} from_json(const json& j)").Append("\n\t\t{");
                for (int i = 0; i < type.Args.Length; i++)
                {
                    codeBuilder.Append($"\n\n\t\t\t{type.Args[i].Type}  {type.Args[i].Name};");
                    codeBuilder.Append($"\n\t\t\tj.at(\"{type.Args[i].Name}\").get_to({type.Args[i].Name});");

                    argBuilder.Append($"{type.Args[i].Name}");
                    if (i < type.Args.Length - 1)
                        argBuilder.Append(", ");
                }

                codeBuilder.Append($"\n\n\t\t\t{type.Name} obj({argBuilder.ToString()});");
                codeBuilder.Append("\n\t\t\treturn obj;");
            }

            codeBuilder.Append("\n\t\t}");

            codeBuilder.Append("\n\t};");

        }
    }

}
