# BufferCodeGen

A .NET Code Emitter for generating Code for Serializing and Deserializing Custom and Third party Types used in Buffer.

I wanted to serialize and Der-serialize my own types and Third-Party type.  Apparently C++ doesn't have fancy stuff like reflection in C# or Java, So I made this very simple Code Generator which uses [nlohmann Json](https://github.com/nlohmann/json) for Serializing and De-Serializing.
It takes a JSON which contains an array of metadata about each Type, which users must define, and Code is generated for nlohmann Json to use it's adl_serializer.

Dependencies:
* [nlohmann Json](https://github.com/nlohmann/json) for Serializing and De-Serializing Json in C++
* [Json .NET](https://www.newtonsoft.com/json) 